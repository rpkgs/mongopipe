---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# mongopipe

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![CRAN status](https://www.r-pkg.org/badges/version/mongopipe)](https://CRAN.R-project.org/package=mongopipe)
<!-- badges: end -->

The goal of mongopipe is to have a simple translator for R code into json based queries for Mongodb.

## Installation

You can install the released version of mongopipe from [CRAN](https://CRAN.R-project.org) with:
``` r
install.packages("mongopipe")
```
or install the development version of mongopipe on [gitlab](https://gitlab.com/rpkgs/mongopipe) with:
``` r
remotes::install_gitlab("illoRocks/mongopipe")
```

## Example

```{r example, message=FALSE}
library(mongopipe)
library(mongolite)
library(nycflights13)

m_flights <- mongo("test_flights", verbose = FALSE)
m_airports <- mongo("test_airports", verbose = FALSE)

if(!m_flights$count())
  m_flights$insert(flights)

if(!m_airports$count())
  m_airports$insert(airports)

result <- mongopipe() %>%
  match(faa="ABQ") %>%
  lookup(from = "test_flights", local_field = "faa", foreign_field = "dest") %>%
  unwind(field = "test_flights") %>%
  project(air_time="$test_flights.air_time", dep_delay="$test_flights.dep_delay", origin="$test_flights.origin") %>%
  m_airports$aggregate()

head(result)
```
```{r}
pipe <- mongopipe() %>%
  match(faa="ABQ") %>%
  lookup(from = "test_flights", local_field = "faa", foreign_field = "dest") %>%
  unwind(field = "test_flights") %>%
  project(air_time="$test_flights.air_time", dep_delay="$test_flights.dep_delay", origin="$test_flights.origin")

print(pipe, pretty = 4)
```


